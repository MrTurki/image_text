<?php
// Create a 300x100 image
$im = imagecreatetruecolor(600, 500);
$red = imagecolorallocate($im, 0xFF, 0x00, 0x00);
$black = imagecolorallocate($im, 0x00, 0x00, 0x00);

// Make the background red
imagefilledrectangle($im, 0, 0, 600, 500, $red);

// Path to our ttf font file
$font_file = './Tahoma.ttf';

// Draw the text 'PHP Manual' using font size 13
imagefttext($im, 43, 0, 105, 55, $black, $font_file, 'PHP Manual');


$text = "Text to print";
$text_color=imagecolorallocate($image,255,255,255);
$font_size = 18;
$space = 10;
$font = "./Tahoma.ttf";
$x=20;
$y=120;
for ($i = 0; $i <strlen($text); $i++){
   $arr = imagettftext ($im, $font_size,0, $x, $y, $text_color, $font, $text{$i});
   $x = $arr[4]+$space;
}


// Output image to the browser
header('Content-Type: image/png');

imagepng($im);
imagedestroy($im);